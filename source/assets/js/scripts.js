//REFACTOR PLS
/* Always namespacing your JS code */
window.holy = {};

(function(window, document, $, holy, undefined){

  "use strict";

  /*
    The holy Object has three core methods the init, initVue and the initPreloadingScreen.

    The init method executes when the document is ready and it contains other methods that will
    be attached to holy Object.

    The iniVue method is initiates the Vue.js framework.

    The initPreloadingScreen is

  */

  holy = window.holy || {};

  holy.init = function(){

    //declaration
    this.initMainMenu = function() {
      var $obj = $('[data-menu-wrapper]');
      //var $close = $()

      if ($obj.length < 1) {
        return;
      }

      $('.nav-icon').on('click', function(){
        var _self = $(this);
        _self.closest('.header-region').find('.main-menu-container').addClass('opened');
        holy.initTweens();
        
      });

      $('[data-menu-close]').on('click', function(){
        var _self = $(this);
        _self.closest('.main-menu-container').removeClass('opened');
      });

    };

    this.initAutoCompletePlaces = function() {
      var _input = document.getElementById('location-input');
      var _options = {
        types: ['(cities)'],
        componentRestrictions: {country: "GR"}
      };
      new google.maps.places.Autocomplete( _input , _options );
    };

    this.initTweens = function() {
      
      TweenMax.from($("[data-tween-1]"), 2, {opacity: 0, bottom: "-5px", ease:Strong.easeInOut, delay: 0.2});
      TweenMax.from($("[data-tween-2]"), 2, {opacity: 0, bottom: "-10px", ease:Strong.easeInOut, delay: 0.4});
      TweenMax.from($("[data-tween-3]"), 2, {opacity: 0, bottom: "-20px", ease:Strong.easeInOut, delay: 0.6});

    };

    //invokation
    holy.initMainMenu();
    holy.initAutoCompletePlaces();

  };
  holy.initVue = function(){
    var app = new Vue({
      el: '#umbrellapp',
      data: {
        place: '',
        cityText: '',
        cleanCityText: '',
        countryCode: 'GR',
        isActiveBgClass: true,
        isNotActiveBgClass: false,
        visible: true,
        hidden: false,
        userLat: '',
        userLong: '',
        cityLocationName: '',
        getLocation: '',
        selectCity: '',
      },
      methods: {
        onSubmit: function() {
          //needs refactor
          var cityName;
          var cleanText;
          this.isActiveBgClass = false;
          this.isNotActiveBgClass = true;
          this.visible= false;
          this.hidden= true;
          this.place = document.getElementById('location-input').value;
          cityName = this.place.split(',');
          this.cityText = cityName[0];
          cleanText = this.cityText.replace(/\s/g,'');
          this.cleanCityText= cleanText;
          //console.log(cityName[0], cleanText);
          this.initAxios();
          return ;
        },
        initNoty: function() {
          new Noty({
            theme: 'relax',
            text: 'You have to login in order to add this city into your favorites!',
            type: 'warning',
            timeout: 4000,
            progressBar: true
          }).show();
          
        },
        initAxios: function(){

          var _app = this;
          var _url = 'http://api.openweathermap.org/data/2.5/forecast/daily?q='+ _app.cleanCityText + ','+ _app.countryCode +'&units=metric&cnt=6&APPID=9ebf87d8944599d92120cfefd976cd20';

          _app.selectCity = true;
          _app.getLocation = false;

          $('.cm-animation-loader').fadeIn('fast');

          holy.initPreloadingScreen();

          setTimeout(function(){
            axios.get(_url)
                .then(function(response){
                  console.log(response.data);

                  var output = '<ul class="list row">';
                  var days = [
                      "SUN",
                      "MON",
                      "TUE",
                      "WED",
                      "THU",
                      "FRI",
                      "SAT"
                  ];

                  for( var i =0; i < response.data.list.length; i++) {

                    var weather = response.data.list[i].weather[0].main;
                    var icon;
                    var timestamp = response.data.list[i].dt;
                    var timeCoverter = function(t) {
                      var a = new Date(t * 1000);
                      var date = a.getDay();
                      return days[a.getDay()];
                    };

                    if(weather === "Clear" || weather === "Extreme") {
                      icon = '<i class="flaticon-sun"></i>';
                    }
                    else if(weather === "Rain") {
                      icon = '<i class="flaticon-rain-cloud"></i>';
                    }
                    else if(weather === "Snow") {
                    icon = '<i class="flaticon-hail"></i>';
                    }
                    else if( weather === "Clouds") {
                      icon = '<i class="flaticon-summer"></i>';
                    }

                    output += '<li class="item">'
                              + '<div class="inner-wrapper">'
                                + '<div class="day-container">' + timeCoverter(timestamp) + '</div>'
                                + '<div class="icm-container">' + icon + '</div>'
                                + '<div class="txt-container color-green">' + response.data.list[i].temp.max  + '&#x2103;  </div>'
                                + '<div class="txt-container color-light-blue">' + response.data.list[i].temp.min + '&#x2103;  </div>'
                              + '</div>'
                          + '</li>';
                  }

                  output += '</ul>';
                  document.querySelector('.response-container').innerHTML = output;
            });
          },1700);

        },
        initAxiosLatLong: function(){

          if(this.userLat && this.userLong) {
            var _app = this;
            _app.selectCity = false;
            _app.getLocation = true;
            this.isActiveBgClass = false;
            this.isNotActiveBgClass = true;
            this.visible= false;
            this.hidden= true;
            var _url = 'http://api.openweathermap.org/data/2.5/forecast/daily?lat='+ this.userLat + '&lon=' + this.userLong + '&units=metric&cnt=6&APPID=9ebf87d8944599d92120cfefd976cd20';
            console.log(_url)
            
            $('.cm-animation-loader').fadeIn('fast');
            
            holy.initPreloadingScreen();
            
            setTimeout(function(){
              axios.get(_url)
                  .then(function(response){
                    console.log(response.data);

                    var output = '<ul class="list row">';
                    var days = [
                        "SUN",
                        "MON",
                        "TUE",
                        "WED",
                        "THU",
                        "FRI",
                        "SAT"
                    ];

                    _app.cityLocationName = response.data.city.name;
                    
                    for( var i =0; i < response.data.list.length; i++) {

                      var weather = response.data.list[i].weather[0].main;
                      var icon;
                      var timestamp = response.data.list[i].dt;
                      var timeCoverter = function(t) {
                        var a = new Date(t * 1000);
                        var date = a.getDay();
                        return days[a.getDay()];
                      };

                      if(weather === "Clear" || weather === "Extreme") {
                        icon = '<i class="flaticon-sun"></i>';
                      }
                      else if(weather === "Rain") {
                        icon = '<i class="flaticon-rain-cloud"></i>';
                      }
                      else if(weather === "Snow") {
                      icon = '<i class="flaticon-hail"></i>';
                      }
                      else if( weather === "Clouds") {
                        icon = '<i class="flaticon-summer"></i>';
                      }

                      output += '<li class="item">'
                                + '<div class="inner-wrapper">'
                                  + '<div class="day-container">' + timeCoverter(timestamp) + '</div>'
                                  + '<div class="icm-container">' + icon + '</div>'
                                  + '<div class="txt-container color-green">' + response.data.list[i].temp.max  + '&#x2103;  </div>'
                                  + '<div class="txt-container color-light-blue">' + response.data.list[i].temp.min + '&#x2103;  </div>'
                                + '</div>'
                            + '</li>';
                    }

                    output += '</ul>';
                    document.querySelector('.response-container').innerHTML = output;
              });            
            },1700);

          }

        },
        initGetLocation: function() {
          var _app = this;

          if (navigator.geolocation) {
              navigator.geolocation.getCurrentPosition(showPosition);
          } else {
              alert("Geolocation is not supported by this browser.");
          }

          function showPosition(position) {

            //console.log( "Latitude: " + position.coords.latitude + "<br>Longitude: " + position.coords.longitude );
            _app.userLat =  position.coords.latitude;
            _app.userLong = position.coords.longitude;
            _app.initAxiosLatLong();
          }
        },
      }
    });
    console.log(app)
  };
  holy.initPreloadingScreen = function() {
      var $obj = $('.cm-animation-loader');
      var $timer = setTimeout(function(){
        $obj.fadeOut('slow');
      }, 2000);
      var $window = $(window);
      if($obj.length < 1) {
        return;
      }

      $window.on('load', function() {
        if($timer) clearTimeout($timer);

        setTimeout(function(){
          $obj.fadeOut('slow');
        }, 2000);
        console.log($timer)
      });

      // $window.on('pageshow', function(event) {
      //   if(event.originalEvent.persisted) __.in.call(_this);
      // })



  };


  // when the window is loaded
  // execute the following methods
  holy.initPreloadingScreen();
  holy.initVue();


  //  when document is ready
  $(function(){
    holy.init();
  });

})(window, document, jQuery, window.holy);